from django.db import models
from django.contrib.auth.models import User

class company(models.Model):
      user = models.OneToOneField(User,on_delete=models.CASCADE)
      Contact_No=models.IntegerField(null= True)
      city=models.CharField(max_length=100,null= True )
      Address=models.TextField(null= True)
      about=models.TextField(null=True,blank=True)
      services=models.TextField(null=True,blank=True)
      headoffice=models.TextField(null=True,blank=True)
      contactperson=models.CharField(max_length=200,null=True,blank=True)
      year=models.CharField(max_length=100,null=True)
      profile_pic= models.ImageField(upload_to='images/%Y/%m/%d',null=True,blank=True)
      Web=models.URLField(null=True)
      
      def staff(self):
          self.user.is_staff = True

      def __str__(self):
        return self.user.username

class student(models.Model):
    gender=(
    ('M','Male'),
    ('F','Female'),
    )
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    Contact_No=models.IntegerField(null=True)
    City=models.CharField(max_length=100,null= True )
    Address=models.TextField(null= True)
    Date_Of_Birth=models.DateField(max_length=100,null= True)
    Gender=models.CharField(max_length=6,choices=gender,default=True)
    country=models.CharField(max_length=200,null=True)
    profile_pic=models.ImageField(upload_to='images/%Y/%m/%d',null=True)

    def __str__(self):
        return self.user.username

class contact(models.Model):
    first=models.CharField(max_length=40)
    last=models.CharField(max_length=40)
    email=models.EmailField()
    phone=models.TextField()
    msg=models.TextField()

    def __str__(self):
        return self.first

class postj(models.Model):
    name = models.ForeignKey(company,on_delete=models.CASCADE)
    position=models.CharField(max_length=200,null=True)
    description=models.TextField(null=True)
    hrname=models.CharField(max_length=30,null=True)
    requirements=models.TextField(null=True)
    salary=models.CharField(max_length=100,null=True)
    #date=models.DateTimeField(auto_now_add=True,null=True)
    city=models.CharField(max_length=200,null=True)
    contact=models.IntegerField(null=True)
    experience=models.CharField(null=True,max_length=200)
    profile_pic= models.ImageField(upload_to='images/%Y/%m/%d',null=True)
   
    def __str__(self):
        return self.position

class addQuestion(models.Model):
    CHOICES=(
        ('a','A'),
        ('b','B'),
        ('c','C'),
        ('d','D')
        )
    question=models.CharField(max_length=500)
    option1=models.CharField(max_length=250)
    option2=models.CharField(max_length=250)
    option3=models.CharField(max_length=250)
    option4=models.CharField(max_length=250)
    answer=models.CharField(max_length=1,choices=CHOICES,blank=True)
    
    created_at=models.DateTimeField(auto_now_add=True,blank=True)

    def __str__(self):
        return self.question

class apply1(models.Model):
    ex=(
        ('0','fresher'),
        ('0-1','0-1 years'),
        ('1-2','1-2 years'),
        ('2-3','2-3 years'),
        ('3-4','3-4 years'),
        ('4-5','4-5 years'),
        ('5-6','5-6 years'),
        ('6-7','6-7 years'),
        ('7-8','7-8 years'),
        ('8-9','8-9 years'),
        ('9-10','9-10 years'),
       
    )
    Student=models.ForeignKey(student,on_delete=models.CASCADE,null=True,blank=True)
    Job=models.ForeignKey(postj,on_delete=models.CASCADE,null=True,blank=True)
    cmpanyid=models.ForeignKey(company,on_delete=models.CASCADE,null=True,blank=True)
    Qualification=models.CharField(max_length=100,null=True)
    Graduation_Percentage=models.DecimalField(max_digits=5,decimal_places=2,null=True)
    Passing_Out_Year=models.IntegerField()
    Resume=models.FileField(upload_to='resume/%Y/%m/%d')
    Experience=models.CharField(choices=ex,max_length=5)
    Apply_on=models.DateTimeField(auto_now_add=True)
    

    def __str__(self):
        return repr(self.Student)
# Create your models here.
